let Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');
Encore
    .setOutputPath('./public/assets')
    .setPublicPath('/assets')

    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSassLoader()
    .enableSourceMaps(!Encore.isProduction())
    // .enableSourceMaps(false)
    .enableVersioning(Encore.isProduction())
    // .enableVersioning(false)
    .disableSingleRuntimeChunk()
    // .enableSingleRuntimeChunk()
    // .splitEntryChunks()
    .autoProvidejQuery()

    // needed to avoid this bug: https://github.com/symfony/webpack-encore/issues/436
    // .configureCssLoader(options => { options.minimize = false; })
    .enablePostCssLoader()

// copy select2 i18n files
    .copyFiles({
            from: './node_modules/select2/dist/js/i18n/',
            // relative to the output dir
            to: 'select2/i18n/[name].[ext]',
            // only copy files matching this pattern
            pattern: /\.js$/
    })

    .addPlugin(
        new CopyWebpackPlugin([
            // default user profile picture
            { from: './assets/images/343c80_828690_3033978.jpg', to: '../upload/images/user_profile' },
            { from: './assets/images/logo_drk.png', to: 'images/logo_drk.png' },
            { from: './assets/images/logo_slv.png', to: 'images/logo_slv.png' }
        ])
    )

    .addEntry('app', [
        './assets/js/app.js',
        './public/bundles/easyadmin/app.js'
    ])

    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
    })



    // enables @babel/preset-env polyfills
    // .configureBabel(() => {}, {
    //     useBuiltIns: 'usage',
    //     corejs: 3
    // })

// uncomment if you use TypeScript
//.enableTypeScriptLoader()

// uncomment to get integrity="..." attributes on your script & link tags
// requires WebpackEncoreBundle 1.4 or higher
//.enableIntegrityHashes()

// uncomment if you use API Platform Admin (composer req api-admin)
//.enableReactPreset()
//.addEntry('admin', './assets/js/admin.js')

;

if(!Encore.isProduction()) {
    Encore.copyFiles({
        from: './node_modules/easy-autocomplete/dist/',
        to: 'easy-autocomplete/[name].[ext]',
        pattern: /\.js$|\.css$/
    })
} else {
    Encore.addPlugin(
        new CopyWebpackPlugin([
            // default user profile picture
            {
                from: './node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.min.js',
                to: 'easy-autocomplete/jquery.easy-autocomplete.js'
            },{
                from: './node_modules/easy-autocomplete/dist/easy-autocomplete.min.css',
                to: 'easy-autocomplete/easy-autocomplete.css'
            },
        ])
    )
}

module.exports = Encore.getWebpackConfig();