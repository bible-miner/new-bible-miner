/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');
require('../css/error.css');
require('../../public/bundles/easyadmin/app.css');
require('../../node_modules/easy-autocomplete/dist/easy-autocomplete.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');
// require('../../node_modules/easy-autocomplete/dist/jquery.easy-autocomplete.js')

// import "easy-autocomplete";

// Auto Close Bootstrap alerts
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 4000);