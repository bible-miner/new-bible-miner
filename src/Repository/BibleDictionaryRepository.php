<?php

namespace App\Repository;

use App\Entity\BibleDictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleDictionary[]    findAll()
 * @method BibleDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleDictionary::class);
    }

    // /**
    //  * @return BibleDictionary[] Returns an array of BibleDictionary objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleDictionary
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
