<?php

namespace App\Repository;

use App\Entity\BibleVerseVSM;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleVerseVSM|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleVerseVSM|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleVerseVSM[]    findAll()
 * @method BibleVerseVSM[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleVerseVSMRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleVerseVSM::class);
    }

//    /**
//     * @return BibleVerseVSM[] Returns an array of BibleVerseVSM objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleVerseVSM
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
