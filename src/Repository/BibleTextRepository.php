<?php

namespace App\Repository;

use App\Entity\BibleText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleText|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleText|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleText[]    findAll()
 * @method BibleText[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleTextRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleText::class);
    }

//    /**
//     * @return BibleText[] Returns an array of BibleText objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleText
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
