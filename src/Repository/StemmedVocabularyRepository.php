<?php

namespace App\Repository;

use App\Entity\StemmedVocabulary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StemmedVocabulary|null find($id, $lockMode = null, $lockVersion = null)
 * @method StemmedVocabulary|null findOneBy(array $criteria, array $orderBy = null)
 * @method StemmedVocabulary[]    findAll()
 * @method StemmedVocabulary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StemmedVocabularyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StemmedVocabulary::class);
    }

//    /**
//     * @return StemmedVocabulary[] Returns an array of StemmedVocabulary objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StemmedVocabulary
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
