<?php

namespace App\Repository;

use App\Entity\BibleVerseStemmedVSM;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BibleVerseStemmedVSM|null find($id, $lockMode = null, $lockVersion = null)
 * @method BibleVerseStemmedVSM|null findOneBy(array $criteria, array $orderBy = null)
 * @method BibleVerseStemmedVSM[]    findAll()
 * @method BibleVerseStemmedVSM[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleVerseStemmedVSMRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BibleVerseStemmedVSM::class);
    }

//    /**
//     * @return BibleVerseStemmedVSM[] Returns an array of BibleVerseStemmedVSM objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BibleVerseStemmedVSM
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
