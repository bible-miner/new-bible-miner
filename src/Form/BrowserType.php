<?php

namespace App\Form;

use App\Entity\BibleBook;
use App\Entity\BibleVersion;
use App\Helper\SearchHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BrowserType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    protected $requestStack;

    protected $searchHelper;

    public function __construct(EntityManagerInterface $em, RequestStack $requestStack, SearchHelper $searchHelper)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->searchHelper = $searchHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'bibleVersion', EntityType::class, [
                    'class' => BibleVersion::class,
                    'label' => false,
                    'choice_label' => 'canonicalName',
                    'choice_value' => 'id',
                    'attr' => ['data-widget' => 'select2']
                ]
            )
            ->add(
                'book', EntityType::class, [
                    'class' => BibleBook::class,
                    'label' => false,
                    'choice_label' => 'canonicalName',
                    'choice_value' => 'id',
                    'placeholder'   => 'tools.browser.book_placeholder',
                    'attr' => ['data-widget' => 'select2']
                ]
            )->add(
                'chapter', ChoiceType::class, [
//                    'class' => BibleBook::class,
                    'label' => false,
                    'choices' => [],
//                    'choice_label' => 'nameAbbreviation',
//                    'choice_value' => 'id',
                    'placeholder'   => 'tools.browser.chapter_placeholder',
                    'disabled' => true,
                    'attr' => ['data-widget' => 'select2']
                ]
            )
            ->add(
                'verse', ChoiceType::class, [
//                    'class' => BibleBook::class,
                    'label' => false,
                    'choices' => [],
//                    'choice_label' => 'nameAbbreviation',
//                    'choice_value' => 'id',
                    'placeholder'   => 'tools.browser.verse_placeholder',
                    'disabled' => true,
                    'attr' => ['data-widget' => 'select2']
                ]
            );

        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    public function onPreSubmit(FormEvent $formEvent)
    {

        $form = $formEvent->getForm();
        $data = $formEvent->getData();

        $form->add(
            'chapter', ChoiceType::class, [
//                    'class' => BibleBook::class,
                'label' => false,
                'choices' => $this->searchHelper->getBookChaptersQueryBuilder(
                    $data['book']
                )->execute()->fetchAll(),
                'choice_name' => 'chapter_number',
//                    'choice_label' => 'nameAbbreviation',
//                    'choice_value' => 'id',
                'placeholder'   => 'tools.browser.chapter_placeholder',
                'attr' => ['data-widget' => 'select2']
            ]
        )
            ->add(
                'verse', ChoiceType::class, [
//                    'class' => BibleBook::class,
                    'label' => false,
                    'choices' => $this->searchHelper->getChapterVersesQueryBuilder(
                        $data['book'], $data['chapter']
                    )->execute()->fetchAll(),
                    'choice_name' => 'verse_number',
//                    'choice_label' => 'nameAbbreviation',
//                    'choice_value' => 'id',
                    'placeholder'   => 'tools.browser.verse_placeholder',
                    'attr' => ['data-widget' => 'select2']
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }

    public function getBlockPrefix()
    {
        return 'browser_form';
    }


}
