<?php

namespace App\Form;

use App\Entity\BibleBook;
use App\Entity\BibleVersion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'bibleVersion', EntityType::class, [
                    'class' => BibleVersion::class,
                    'label' => false,
                    'choice_label' => 'canonicalName',
                    'choice_value' => 'id',
                    'attr' => ['data-widget' => 'select2']
                ]
            )
            ->add(
                'searchSummary', CheckboxType::class, [
                    'label' => 'tools.summary.button_label',
                    'required' => false
                ]
            )
            ->add(
                'searchTerms', TextType::class, [
                    'label' => false,
                    'attr' => ['placeholder'=>'tools.search.placeholder']
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }

    public function getBlockPrefix()
    {
        return 'search_form';
    }


}
