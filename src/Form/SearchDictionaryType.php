<?php

namespace App\Form;

use App\Entity\BibleDictionaryData;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchDictionaryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add(
//                'searchReference', EntityType::class, [
//                    'class' => BibleDictionaryData::class,
//                    'label' => false,
//                    'choice_label' => 'reference',
//                    'choice_value' => 'id',
//                    'placeholder'   => 'tools.search.dict_placeholder',
//                    'attr' => ['data-widget' => 'select2']
//                ]
//            )
            ->add(
                'searchReference', TextType::class, [
                    'label' => false,
                    'attr' => [
                        'placeholder'=>'tools.search.dict_placeholder'
                    ]
                ]
            )
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }

    public function getBlockPrefix()
    {
        return 'search_dictionary_form';
    }
}
