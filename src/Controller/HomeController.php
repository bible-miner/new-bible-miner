<?php

namespace App\Controller;

use App\Entity\BibleBook;
use App\Entity\BibleText;
use App\Helper\SearchHelper;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends BaseController
{
    protected $searchHelper;


    public function __construct(SearchHelper $searchHelper)
    {
        $this->searchHelper = $searchHelper;
    }

    /**
     * @Route("/", name="home_index")
     */
    public function indexAction(Request $request)
    {
        return $this->redirectToRoute(
            'home_tools',
            ['menuIndex'=> 0, 'submenuIndex'=> -1]
        );
    }

    /**
     * @Route("/home/tools", name="home_tools")
     */
    public function toolsAction(Request $request)
    {
//        $em = $this->getDoctrine()->getManager();
//
//        $qb = $em->getRepository(BibleText::class)
//            ->createQueryBuilder('bt');
//
//        $qb->where($qb->expr()->eq('bt.chapterNumber', 1))
//            ->andWhere($qb->expr()->lt('bt.verseNumber', 4))
//            ->innerJoin('bt.book', 'bb')
//            ->innerJoin('bt.version', 'bv');
//
//        $bibleVerses  = $this->get('easyadmin.paginator')->createOrmPaginator(
//            $qb, $request->query->getInt('page', 1), 9
//        );

        return $this->render(
            'home/tools.html.twig', [
                'browserForm' => $this->getBrowserForm([])->createView(),
                'searchForm' => $this->getSearchForm([])->createView(),
                'dictSearchForm' => $this->getDictSearchForm([])->createView(),
                'summaryForm' => $this->getSummaryForm([])->createView()
            ]
        );
    }

    /**
     * @Route("/home/dashboard", name="home_dashboard")
     */
    public function dashboardAction()
    {
        return $this->render('home/dashboard.html.twig');
    }

    /**
     * @Route("/home/privacy", name="home_privacy")
     */
    public function privacyAction()
    {
        return $this->render(
            'home/privacy_policy.html.twig'
        );
    }

    /**
     * @Route("/home/help", name="home_help")
     */
    public function helpAction()
    {
        return $this->render('home/help.html.twig');
    }

    /**
     * @Route("/home/about", name="home_about")
     */
    public function aboutAction()
    {
        return $this->render('home/about.html.twig');
    }
}
