<?php

namespace App\Controller;

use App\Entity\BibleText;
use App\Entity\BibleVersion;
use App\Helper\SearchHelper;
use Phpml\FeatureExtraction\TextRank\TextRankFacade;
use Phpml\FeatureExtraction\TextRank\Tool\Graph;
use Phpml\FeatureExtraction\TextRank\Tool\Parser;
use Phpml\FeatureExtraction\TextRank\Tool\Score;
use Phpml\FeatureExtraction\TextRank\Tool\StopWords\English;
use Phpml\FeatureExtraction\TextRank\Tool\StopWords\Spanish;
use Phpml\FeatureExtraction\TextRank\Tool\Summarize;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TextExtractionController extends BaseController
{
    /**
     * @var SearchHelper
     */
    protected $searchHelper;

    public function __construct(SearchHelper $searchHelper)
    {
        $this->searchHelper = $searchHelper;
    }


    /**
     * @Route("/extraction/summary/book",
     *     name="text_extraction_book_summary"
     * )
     */
    public function bookSummaryAction(Request $request)
    {
        $form = $this->getSummaryForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $queryBuilder = $this->getDoctrine()
                ->getRepository(BibleText::class)
                ->createQueryBuilder('brc');

            $queryBuilder->select('brc.verseText')
                ->join('brc.version', 'bv')
                ->where($queryBuilder->expr()->eq('brc.book', $form->get('book')
                    ->getData()->getId())
                )->andWhere(
                    $queryBuilder->expr()->eq('bv.id', $form->get('bibleVersion')
                        ->getData()->getId())
                )
//                ->andWhere($queryBuilder->expr()->eq('brc.chapterNumber', $request->get('chapter')))
//                ->groupBy('brc.book, brc.chapterNumber, brc.verseText')
                ->orderBy('brc.id');
            $result = sprintf("%s",
                implode(
                    " ",
                    array_map('current', $queryBuilder->getQuery()->getArrayResult()))
            );

            $stopWords = ($request->getLocale() == 'es') ?
                new Spanish() : new English();

            return $this->render(
                'text_extraction/summary.html.twig', [
                    'bibleVersion' => $this->getDoctrine()
                        ->getRepository(BibleVersion::class)
                        ->find($form->get('bibleVersion')
                            ->getData()->getId()),
                    'book' => $form->get('book')->getData(),
                    'summary' => $this->getCustomSummary(
                        $result,
                        $form->get('percent')->getData(),
                        $stopWords
                    ),
                    'keyWords' => $this->getCustomSummary(
                        $result,
                        $form->get('percent')->getData(),
                        $stopWords, true
                    ),
                ]
            );
        } else {
            return $this->redirectToRoute('home_index');
        }
//            dump($textRank->getOnlyKeyWords($result));die;

    }

    /**
     * @Route("/extraction/summary/search",
     *     name="text_extraction_search_summary"
     * )
     */
    public function searchSummaryAction(Request $request)
    {
        $rankedResults = $this->searchHelper
            ->getSuperRankedSearchQueryBuilder(
                array_keys($request->get('relevantTerms')),
                $request->get('bibleVersion')
            )
            ->setMaxResults(100)->execute()->fetchAll();
        $textResult = implode (
            " ",
            array_map(
                function ($record) {
                    return $record['verse_text'];
                }, $rankedResults
            )
        );

        $textRank = new TextRankFacade();
        $stopWords = ($request->getLocale() == 'es') ?
            new Spanish() : new English();
        $textRank->setStopWords($stopWords);
        return $this->render(
            'text_extraction/summary.html.twig', [
                'bibleVersion' => $this->getDoctrine()
                    ->getRepository(BibleVersion::class)
                    ->find($request->get('bibleVersion')),
                'pageTitle' => 'tools.summary.title',
                'summary' => $this->getCustomSummary(
                    $textResult,
                    30,
                    $stopWords
                ),
                'keyWords' => $this->getCustomSummary(
                    $textResult,
                    30,
                    $stopWords, true
                ),
            ]
        );
    }

    protected function getCustomSummary(string $rawText, $percent, $stopWords, $onlyKeywords = false)
    {
        $parser = new Parser();
        $parser->setMinimumWordLength(4);
        $parser->setRawText($rawText);
        $parser->setStopWords($stopWords);

        $text = $parser->parse();
        $maximumSentences = (int) (count($text->getSentences()) * ($percent / 100));

        $graph = new Graph();
        $graph->createGraph($text);

        $score = new Score();
        $scores = $score->calculate($graph, $text);

        if($onlyKeywords) {
            return $scores;
        }

        $summarize = new Summarize();

        return $summarize->getSummarize(
            $scores,
            $graph,
            $text,
            12,
            $maximumSentences,
            Summarize::GET_ALL_IMPORTANT
        );
    }
}
