<?php

namespace App\Controller\Admin;


use App\Controller\BaseController;
use App\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends BaseController
{
    protected $userManager;
    protected $passwordEncoder;

    public function __construct(UserManagerInterface $userManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userManager = $userManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function createNewEntity()
    {
        return $this->userManager->createUser();
    }

    protected function updateEntityProperty($entity, $property, $value)
    {
        $id = (int) $this->request->query->get('id');
        $currentUserId = $this->get('security.token_storage')
            ->getToken()->getUser()->getId();

        if ($id === $currentUserId) {
            throw new \RuntimeException(\sprintf('Cannot disable current logged user.'));
        }
        parent::updateEntityProperty($entity, $property, $value);
    }

    /**
     * @param User $entity
     */
    protected function updateEntity($entity)
    {
        $this->userManager->updateUser($entity, false);
        $plainPassword = $this->request->request->get(
            strtolower($this->entity['name']))['plainPassword'];
        if(strlen($plainPassword) !== 0) {
            $entity->setPlainPassword($plainPassword);
            $this->userManager->updatePassword($entity);
        }
        parent::updateEntity($entity);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|void
     * @throws \Exception
     */
    protected function deleteAction()
    {
        $id = (int) $this->request->query->get('id');
        $currentUserId = $this->get('security.token_storage')
            ->getToken()->getUser()->getId();
        if ($id !== $currentUserId) {
            return parent::deleteAction();
        } else {
            throw new \RuntimeException('Current Logged-in user cannot be deleted.');
        }
    }
}