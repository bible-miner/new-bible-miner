<?php

namespace App\Controller;

use App\Controller\Admin\UserController;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserProfileController extends UserController
{
    /**
     * @Route("/user/profile", name="user_profile")
     */
    public function userProfileAction(Request $request)
    {
        return $this->redirectToRoute(
            'easyadmin', [
                'action' => 'show',
                'entity' => 'UserProfile',
                'id' => $this->get('security.token_storage')->getToken()->getUser()->getId(),
                'menuIndex' => $request->query->get('menuIndex'),
                'submenuIndex' => $request->query->get('submenuIndex')
            ]
        );
    }

    /**
     * @param User $entity
     */
    protected function updateEntity($entity)
    {
        if ($this->request->query->get('id') != $this->get('security.token_storage')
                ->getToken()->getUser()->getId()) {
            throw new AccessDeniedException();
        }

        $plainPassword = $this->request->request->get(
            strtolower($this->entity['name']))['plainPassword'];
        $currentPlainPassword = $this->request->request->get(
            strtolower($this->entity['name']))['currentPlainPassword'];
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($this->passwordEncoder->isPasswordValid($user, $currentPlainPassword)) {
            $entity->setPlainPassword($plainPassword);
            $this->userManager->updateUser($entity);
            parent::updateEntity($entity);
            $this->addFlash('success', 'Password changed successfully');
        } else {
            $this->addFlash('warning', 'Current password mismatch');
            $this->redirectToReferrer();
        }
    }

    protected function showAction()
    {
        if ($this->request->query->get('id') != $this->get('security.token_storage')
                ->getToken()->getUser()->getId()) {
            throw new AccessDeniedException();
        }

        return parent::showAction();
    }
}
