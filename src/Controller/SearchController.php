<?php

namespace App\Controller;

use App\Entity\BibleDictionaryData;
use App\Entity\BibleVersion;
use App\Helper\PaginatorHelper;
use App\Helper\SearchHelper;
use Phpml\FeatureExtraction\TextRank\Tool\StopWords;
use Phpml\Tokenization\WordTokenizer;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Wamania\Snowball as Stemmer;

class SearchController extends BaseController
{
    /**
     * @var SearchHelper
     */
    protected $searchHelper;

    protected $csrfTokenManager;

    protected $stemmer;

    public function __construct(SearchHelper $searchHelper, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->searchHelper = $searchHelper;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @Route("/search/ranked", name="ranked_search")
     */
    public function rankedSearchAction(Request $request)
    {
        if($request->getLocale() == 'es') {
            $this->stemmer = new Stemmer\Spanish();
            $stopWords = new StopWords\Spanish();
        } elseif ($request->getLocale() == 'en') {
            $this->stemmer = new Stemmer\English();
            $stopWords = new StopWords\English();
        }

        $form = $this->getSearchForm();
        $form->handleRequest($request);

        if(!$form->isSubmitted()) {
            $csrfToken = $this->csrfTokenManager->getToken('search_form');
            $submitData = [
                'searchTerms' => $request->query->get('searchTerms'),
                '_token' => $csrfToken->getValue(),
                'bibleVersion' => $request->query->get('bibleVersion'),
                'searchSummary' => $request->query->getBoolean('searchSummary')
            ];
            $form->submit($submitData);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $wordTokenizer = new WordTokenizer();
            $searchTerms = $wordTokenizer->tokenize($form->get('searchTerms')->getData());

            $stemmedTerms = [];
            foreach ($searchTerms as $searchTerm) {
                if(!$stopWords->exist(strtolower($searchTerm))) {
                    $stemmedTerms[$this->stemmer->stem($searchTerm)] = $searchTerm;
                }
            }

            $termScores = $this->searchHelper
                ->getStemmedTermsScore(array_keys($stemmedTerms));

            $relevantTerms = [];
            $relevantWords = [];
            foreach ($termScores as $stemmedTerm => $termScore) {
                $relevantTerms[$stemmedTerm] = [
                    'word' => strtolower($stemmedTerms[$stemmedTerm]),
                    'score' => round(10 - (float) $termScore['idf'],3)
                ];
                $relevantWords[] = strtolower($stemmedTerms[$stemmedTerm]);
            }

            if (isset($relevantTerms) && !$form->get('searchSummary')->getData()) {
                $rankedResults = $this->searchHelper
                    ->getSuperRankedSearchQueryBuilder(
                        array_keys($relevantTerms),
                        $form->get('bibleVersion')->getData()->getId()
                    );
            } elseif (isset($relevantTerms) && $form->get('searchSummary')->getData() == true) {
                return $this->redirectToRoute(
                    'text_extraction_search_summary',[
                        'relevantTerms' => $relevantTerms,
                        'relevantWords' => $relevantWords,
                        'bibleVersion' => $form->get('bibleVersion')->getData()->getId()
                    ]
                );
            }

            $paginator = new PaginatorHelper();
            $bibleVerses = $paginator->createArrayPaginator(
                $rankedResults->execute()->fetchAll(\PDO::FETCH_ASSOC),
                $request->query->getInt('page', 1), 15
            );

            return $this->render(
                'search/index.html.twig', [
                    'relevantWords' => $relevantWords,
                    'relevantTerms' => $relevantTerms,
                    'paginator' => $bibleVerses
                ]
            );
        }
        return $this->redirectToRoute('home_index');
    }

    /**
     * @Route("/search/dictionary", name="dictionary_search")
     */
    public function dictionaryReferenceSearchAction(Request $request, TranslatorInterface $translator)
    {
        $form = $this->getDictSearchForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $reference = $form->get('searchReference')->getData();
            $qb = $this->getDoctrine()->getManager()
                ->getRepository(BibleDictionaryData::class)
                ->createQueryBuilder('bdd')
                ->join('bdd.dictionary', 'bd')
                ->select('bd.canonicalName', 'bd.shortName', 'bdd.reference', 'bdd.definition');

            $qb->where(
                $qb->expr()->like('bdd.reference', ':reference')
            )
            ->setParameter('reference', $reference)
            ->groupBy('bdd.dictionary')
            ->addGroupBy('bdd.reference')
            ->addGroupBy('bdd.definition');

            $dictionaryResults = $qb->getQuery()->getArrayResult();

            if(count($dictionaryResults) > 0) {
                return $this->render(
                    'search/dictionary.html.twig', [
                        'dictionaryResults' => $dictionaryResults
                    ]
                );
            } else {
                $this->addFlash(
                    'warning',
                    $translator->trans('search.no_results', [], 'EasyAdminBundle')
                );
                return $this->redirectToRoute('home_index');
            }

        }
        return $this->redirectToRoute('home_index');
    }

    /**
     * @Route("/search/references", name="dictionary_references_search")
     */
    public function getDictionaryReferenceAction(Request $request)
    {
        $qb = $this->getDoctrine()->getManager()
            ->getRepository(BibleDictionaryData::class)
            ->createQueryBuilder('bdd')
            ->select('DISTINCT bdd.reference');

        $qb->where(
            $qb->expr()->like('bdd.reference', ':reference')
        )
            ->orWhere(
                $qb->expr()->like('bdd.stemmedReference', ':reference')
            )
            ->setParameter('reference', $request->get('phrase'). '%');

        return new JsonResponse($qb->getQuery()->getArrayResult());
    }

    /**
     * @param Request $request
     * @Route("/search/chapters", name="chapters_search")
     * @return JsonResponse
     */
    public function getBibleChaptersAction(Request $request)
    {
        $queryBuider = $this->searchHelper->getBookChaptersQueryBuilder(
            $request->get('bookId')
        );
        return new JsonResponse($queryBuider->execute()->fetchAll());
    }

    /**
     * @param Request $request
     * @Route("/search/verses", name="verses_search")
     * @return JsonResponse
     */
    public function getBibleVersesAction(Request $request)
    {
        $queryBuider = $this->searchHelper->getChapterVersesQueryBuilder(
            $request->get('bookId'),
            $request->get('chapterNumber')
        );
        return new JsonResponse($queryBuider->execute()->fetchAll());
    }
}
