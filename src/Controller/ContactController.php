<?php

namespace App\Controller;

use App\Entity\Contact;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends BaseController
{
    protected $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        return $this->redirectToRoute(
            'easyadmin', [
                'action' => 'new',
                'entity' => 'Contact',
                'menuIndex' => $request->query->get('menuIndex'),
                'submenuIndex' => $request->query->get('submenuIndex')
            ]
        );
    }

    protected function persistEntity($entity)
    {
        $this->sendContactEmail($entity);
        parent::persistEntity($entity);
    }

    protected function sendContactEmail(Contact $entity)
    {
        $message = (new \Swift_Message('Bible-Miner: '. $entity->getSubject()))
            ->setFrom($entity->getFromEmail())
            ->setTo('webmaster@fluency-labs.com')
            ->setBody(
                "From: {$entity->getContactName()} <{$entity->getFromEmail()}>\n\n" .
                $entity->getMessage(),
                'text/plain'
            );
        try {
            $this->mailer->send($message);
            $this->addFlash('success', 'It sent!');
        } catch (\Exception $e) {
            $this->addFlash('danger', $e->getMessage());
        }
    }
}
