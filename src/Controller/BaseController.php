<?php

namespace App\Controller;

use App\Entity\BibleBook;
use App\Entity\BibleVersion;
use App\Form\BrowserType;
use App\Form\SearchDictionaryType;
use App\Form\SearchType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class BaseController extends EasyAdminController
{
    protected function resultsForSelect2(array $entities) {
        $results = [];

        foreach ($entities as $entity) {
            $results[] = [
                'id' => $entity->getId(),
                'name' => (string) $entity,
            ];
        }

        return $results;
    }

    protected function getSearchForm(array $data = [])
    {
        $form = $this->createForm(
            SearchType::class, $data , [
                'action' => $this->generateUrl('ranked_search'),
                'method' => 'get'
            ]
        );

        return $form;
    }

    protected function getBrowserForm(array $data = [])
    {
        $form = $this->createForm(
            BrowserType::class, $data , [
                'action' => $this->generateUrl('book_index'),
                'method' => 'get'
            ]
        );

        return $form;
    }

    protected function getSummaryForm(array $data = [])
    {
        return $this->get('form.factory')->createNamedBuilder(
            'summary_form', FormType::class, $data
        )
            ->add(
                'bibleVersion', EntityType::class, [
                    'class' => BibleVersion::class,
                    'label' => false,
                    'choice_label' => 'canonicalName',
                    'choice_value' => 'id',
                    'attr' => ['data-widget' => 'select2']
                ]
            )
            ->add(
                'book', EntityType::class, [
                    'class' => BibleBook::class,
                    'label' => false,
                    'choice_label' => 'canonicalName',
                    'choice_value' => 'id',
                    'placeholder'   => 'tools.browser.book_placeholder',
                    'attr' => ['data-widget' => 'select2']
                ]
            )
            ->add(
                'percent', IntegerType::class,[
                    'attr' => [
                        'class' => 'input-number', 'value' => 30,'min'=> 10, 'max' => 80
                    ],
                    'label' => 'Resumen (%)'
                ]
            )
            ->setAction($this->generateUrl('text_extraction_book_summary'))
            ->setMethod('get')->getForm();
    }

    protected function getDictSearchForm(array $data = [])
    {
        $form = $this->createForm(
            SearchDictionaryType::class, $data , [
                'action' => $this->generateUrl('dictionary_search'),
                'method' => 'get'
            ]
        );

        return $form;
    }
}
