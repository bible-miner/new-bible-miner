<?php

namespace App\Controller;

use App\Entity\BibleBook;
use App\Entity\BibleText;
use App\Helper\SearchHelper;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BookController extends BaseController
{
    protected $searchHelper;


    public function __construct(SearchHelper $searchHelper)
    {
        $this->searchHelper = $searchHelper;
    }
    /**
     * @Route("/book/{id}", name = "book_index", defaults={"id"="1|2"} )
     */
    public function indexAction(Request $request)
    {
        $form = $this->getBrowserForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $bookId = $form->get('book')->getData()->getId();
            $versionId = $form->get('bibleVersion')->getData()->getId();
        } else {
            $bookId = $request->get('id');
            $versionId = $request->get('bibleVersion');
        }

        $em = $this->getDoctrine()->getManager();

        $qbBibleBook = $em->getRepository(BibleBook::class)
            ->createQueryBuilder('bb');

        $qbBibleBook->where($qbBibleBook->expr()->eq('bb.id', ':id'))
            ->setParameter('id', $bookId);

        try {
            $bibleBook = $qbBibleBook->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        if ($form->isSubmitted() && $form->isValid())
        {
            $chapter = $form->get('chapter')->getData();
            $verse = $form->get('verse')->getData();

            $page = $this->searchHelper->calculateVersePage(
                $bookId,
                $chapter,
                $verse,
                $versionId
            );

            return $this->redirect(
                $this->generateUrl(
                    'book_index',
                    ['id'=>$bookId]
                ).'?page='.$page.'&bibleVersion='. $versionId.
                '#'.$bibleBook->getNameAbbreviation(). $chapter.$verse
            );
        } elseif (!$form->isSubmitted() && $request->get('chapter') &&
            $request->get('verse') && $request->get('bibleVersion')
        ) {
            $chapter = $request->get('chapter');
            $verse = $request->get('verse');
            $page = $this->searchHelper->calculateVersePage(
                $bookId,
                $chapter,
                $verse,
                $versionId
            );
            return $this->redirect(
                $this->generateUrl(
                    'book_index',
                    ['id'=>$bookId]
                ).'?page='.$page.'&bibleVersion='. $versionId.
                '#'.$bibleBook->getNameAbbreviation(). $chapter.$verse
            );
        }

        $qbVerses = $em->getRepository(BibleText::class)
            ->createQueryBuilder('bt');

        $qbVerses->select('bt, bb, bv')
            ->where($qbVerses->expr()->eq('bb.id', ':book_id'))
            ->andWhere($qbVerses->expr()->eq('bv.id', ':version_id'))
            ->setParameter('book_id', $bookId)
            ->setParameter('version_id', $versionId)
            ->innerJoin('bt.book', 'bb')
            ->innerJoin('bt.version', 'bv');

        $bibleVerses = $this->get('easyadmin.paginator')->createOrmPaginator(
            $qbVerses,
            $request->query->getInt('page', 1), 15
        );

        return $this->render(
            'book/index.html.twig', [
                'book' => $bibleBook,
                'paginator' => $bibleVerses
            ]
        );
    }
}
