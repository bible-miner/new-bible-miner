<?php

namespace App\Command;

use App\Entity\BibleText;
use App\Entity\StemmedVocabulary;
use App\Entity\Vocabulary;
use App\Helper\DatabaseHelper;
use Doctrine\ORM\EntityManagerInterface;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Tokenization\WordTokenizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BibleIndexExtractVocabularyCommand extends Command
{
    protected static $defaultName = 'bible:index:extract-vocabulary';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DatabaseHelper
     */
    protected $databaseHelper;

    public function __construct(?string $name = null, EntityManagerInterface $em, DatabaseHelper $databaseHelper)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->databaseHelper = $databaseHelper;
    }

    protected function configure()
    {
        $this
            ->setDescription('Extract Vocabulary')
            ->addArgument(
                'version', InputArgument::OPTIONAL,
                'Version shortname (\'RVG\', \'RV1909\',..).',
                'RVG'
            )
            ->addOption(
                'stemmed', 's', InputOption::VALUE_OPTIONAL,
                'Use stemmed words.', 1
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $bibleRepository = $this->em->getRepository(BibleText::class);
        $vocabularyEntity = new Vocabulary();
        $stemmedVocabularyEntity = new StemmedVocabulary();

        if($input->getOption('stemmed') == 1) {
            $vocabularyWordTokenizer = new TokenCountVectorizer(new WordTokenizer());

            $bibleVerseTokensQueryBuilder = $bibleRepository->createQueryBuilder('brc');

            $bibleVerseTokens = array_map(
                'current', $bibleVerseTokensQueryBuilder
                ->select('brc.stemmedVerseTokens')
                    ->getQuery()->getArrayResult()
            );

            $bibleVerseTokensQueryBuilder->resetDQLPart('select')
                ->select('brc');
            $vocabularyWordTokenizer->fit($bibleVerseTokens);

            $bibleVocabulary = $vocabularyWordTokenizer->getVocabulary();

            $io->warning('Truncating stemmed vocabulary table.');
            $this->databaseHelper->truncateTable($stemmedVocabularyEntity);

            $io->note('Processing stemmed unique terms.');
            $io->progressStart(count($bibleVocabulary));
            foreach ($bibleVocabulary as $token) {
                $entity = clone $stemmedVocabularyEntity;
                $entity->setStemmedWord($token);
                $entity->setDocumentFreq(0);
                $entity->setCollectionFreq(0);
                $entity->setIdfValue(0);
                $this->em->persist($entity);
                $io->progressAdvance();
            }
            $this->em->flush();
            $io->progressFinish();
        } else {
            $vocabularyWordTokenizer = new TokenCountVectorizer(new WordTokenizer());


            $bibleVerseTokens = $bibleRepository->createQueryBuilder('brc')
                ->select('brc.verseTokens')->getQuery()->getArrayResult();

            $bibleVerseTokens = array_map('current', $bibleVerseTokens);
            $vocabularyWordTokenizer->fit($bibleVerseTokens);

            $bibleVocabulary = $vocabularyWordTokenizer->getVocabulary();

            $io->warning('Truncating vocabulary table.');
            $this->databaseHelper->truncateTable($vocabularyEntity);

            $io->note('Processing unique terms.');
            $io->progressStart(count($bibleVocabulary));
            foreach ($bibleVocabulary as $token) {
                $entity = clone $vocabularyEntity;
                $entity->setWord($token);
                $entity->setDocumentFreq(0);
                $entity->setCollectionFreq(0);
                $entity->setIdfValue(0);
                $this->em->persist($entity);
                $io->progressAdvance();
            }
            $this->em->flush();
            $io->progressFinish();
        }
    }
}
