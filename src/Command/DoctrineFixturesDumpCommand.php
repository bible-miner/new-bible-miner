<?php

namespace App\Command;

use App\Entity\BibleBook;
use App\Entity\BibleDictionary;
use App\Entity\BibleDictionaryData;
use App\Entity\BibleText;
use App\Entity\BibleVersion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

class DoctrineFixturesDumpCommand extends Command
{
    protected static $defaultName = 'doctrine:fixtures:dump';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct($name = null, EntityManagerInterface $em)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Dump tables from database to yaml format.')
            ->addArgument('filename', InputArgument::REQUIRED, 'filename')
            ->addOption(
                'filepath', null, InputOption::VALUE_OPTIONAL,
                'Filepath', 'src/DataFixtures/fixtures'
            )
            ->addOption(
                'entity-class', null, InputOption::VALUE_REQUIRED,
                'Entity class name, use \\\\ for FQDN class.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $queryBuilder = $this->em->createQueryBuilder()
            ->from($input->getOption('entity-class'), 'ec')
            ->orderBy('ec.id', 'ASC');
        if($input->getOption('entity-class') == BibleText::class) {
            $queryBuilder->join(BibleVersion::class, 'bv')
                ->join(BibleBook::class, 'bb')
                ->select(
                    'ec.chapterNumber', 'ec.verseNumber', 'ec.verseReference', 'ec.strong',
                    'ec.verseText', 'ec.verseText', 'ec.verseTokens', 'ec.stemmedVerseTokens',
                    'bv.nameAbbreviation as bibleVersion', 'bb.nameAbbreviation as bibleBook'
                );
        } elseif ($input->getOption('entity-class') == BibleVersion::class) {
            $queryBuilder->select(
                'ec.canonicalName', 'ec.nameAbbreviation', 'ec.language',
                'ec.description'
            );
        } elseif ($input->getOption('entity-class') == BibleDictionary::class) {
            $queryBuilder
                ->select(
                    'ec.canonicalName', 'ec.shortName', 'ec.description', 'ec.comments',
                    'ec.author', 'ec.strong', 'ec.version', 'ec.versionDate', 'ec.publishDate',
                    'ec.rightToLeft', 'ec.language'
                );
        } elseif ($input->getOption('entity-class') == BibleDictionaryData::class){
            $queryBuilder->join(BibleDictionary::class, 'bd')
                ->select(
                    'ec.reference', 'ec.stemmedReference', 'ec.definition',
                    'bd.shortName as dictionary'
                );
        }
        else {
            $queryBuilder->select('ec');
        }
        $records = [];
        $records[$input->getOption('entity-class')] = $queryBuilder->getQuery()->getArrayResult();
        $filePathName = $input->getOption('filepath') . DIRECTORY_SEPARATOR .
            $input->getArgument('filename');
        file_put_contents(
            $filePathName, Yaml::dump($records, 3, 4)
        );
        $io->success(
            sprintf(
                'Database content for %s successfully dumped to %s',
                $input->getOption('entity-class'), $filePathName
            )
        );
    }
}
