<?php

namespace App\Command;

use App\Entity\BibleDictionary;
use App\Entity\BibleDictionaryData;
use Doctrine\ORM\EntityManagerInterface;
use Phpml\Tokenization\WordTokenizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Yaml\Yaml;
use Wamania\Snowball as Stemmer;

class BibleDictionaryScrapCommand extends Command
{
    protected static $defaultName = 'bible:dictionary:scrap';

    /**
     * @var string
     */
    protected $baseUrl = "https://www.bibliatodo.com/Diccionario-biblico";

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ScrapBibleDictionaryCommand constructor.
     * @param null $name
     * @param EntityManagerInterface $em
     */
    public function __construct($name = null, EntityManagerInterface $em)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('search', InputArgument::OPTIONAL, 'Search query.')
            ->addOption(
                'get-links',null,InputOption::VALUE_NONE,
                'Get words search links.'
            )
            ->addOption(
                'scrap-links',null,InputOption::VALUE_NONE,
                'Given an array [word => searchLink...[]] scrap and save content to local database.'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getOption('get-links') && !$input->getOption('scrap-links')) {

            $this->getSearchWordsLinks($io);
        }
        elseif (!$input->getOption('get-links') && $input->getOption('scrap-links')) {
            $this->scrapAndPersisContent($io);
        }
        else {
            $searchQuery = $input->getArgument('search');
            $this->scrapOnline($io, $searchQuery);
        }


    }


    protected function scrapAndPersisContent(SymfonyStyle $io)
    {
        $linksToScrap = Yaml::parse(
            file_get_contents('data/reference/node_links_unique.yaml')
        );

        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $offSet = (int) file_get_contents('data/reference/offset');
        $linksToScrap = array_slice($linksToScrap , $offSet);
        $io->progressStart(count($linksToScrap));
        foreach ($linksToScrap as $word => $link) {
            $crawler = $goutteClient->request('GET', $link);
//            dump($link);die;
            if ($crawler->filterXPath('//*[@id="imprimible"]')->count() > 0) {
                $resultsContent = $crawler->filterXPath('//*[@id="imprimible"]')->children()
                    ->each(
                        function (Crawler $node, $i) {
                            if (!empty($node->html())) {
                                return [$node->nodeName() => trim($node->text())];
                            }
                        }
                    );

                $resultsContent = array_values(array_filter($resultsContent));

                $hydratedContent = [];
                foreach ($resultsContent as $resultContent) {
                    if (array_key_exists('h5', $resultContent)) {
                        $dictionaryName = explode(' - ', $resultContent['h5']);
                        if (count($dictionaryName) > 1) {
                            $title = $resultContent['h5'];
                        }
                    } else {
                        $hydratedContent[$title][] = array_shift($resultContent);
                    }
                }

                foreach ($hydratedContent as $title => $paragraphs) {
                    $dictionaryName = explode(' - ', $title);
                    if (count($dictionaryName) > 1) {
                        $dictionaryName = $dictionaryName[1];
                    } else {
                        dump([$dictionaryName, $word, $link]);
                    }


                    $dictionary = $this->getDictionary($dictionaryName);

                    $wordTokenizer = new WordTokenizer();
                    $wordsArray = array_map(
                        'strtolower', $wordTokenizer->tokenize($word)
                    );
                    if ($dictionary->getLanguage() == 'es') {
                        $stemmer = new Stemmer\Spanish();
                    } else {
                        $io->error('Language not supported.');
                        exit();
                    }
                    $stemmedReference = array_filter(array_map(
                        function ($word) use ($stemmer) {
                            return $stemmer->stem($word);
                        }, $wordsArray
                    ));

                    $dictionaryDataEntity = new BibleDictionaryData();
                    $dictionaryDataEntity->setDictionary($dictionary);
                    $dictionaryDataEntity->setReference(trim(mb_convert_case($word, MB_CASE_TITLE, "UTF-8")));
                    $dictionaryDataEntity->setStemmedReference(implode(" ", $stemmedReference));
                    $dictionaryDataEntity->setDefinition("<p>" . implode("</p><p>", $paragraphs) . "</p>");
                    $dictionaryDataEntity->setSource($link);
                    $this->em->persist($dictionaryDataEntity);
                }
                $this->em->flush();
            } else {
                $content = file_get_contents('data/reference/import.log');
                $content .= "\r\n" . sprintf("Line %d: '%s' => '%s'", ($offSet + 1), $word, $link);
                file_put_contents('data/reference/import.log', $content);
            }
            $offSet++;
            file_put_contents('data/reference/offset', $offSet);
            $io->progressAdvance();
        }
        $io->progressFinish();
        $io->success('Done');
    }

    protected function getSearchWordsLinks(SymfonyStyle $io) {
        $nonLinks = [];
        $withLinks = [];
        $wordsList = $this->getWordsList();
        $io->progressStart(count($wordsList));
        foreach ($wordsList as $word) {
            $goutteClient = new Client();
            $guzzleClient = new GuzzleClient(array(
                'timeout' => 60,
            ));
            $goutteClient->setClient($guzzleClient);

            $crawler = $goutteClient->request('GET', $this->baseUrl.'/'.'?s='.$word);

            $nodeLinks = $crawler->filterXPath('//*[@id="grupo"]')
                ->filter('p')
                ->filter('a')
                ->each(
                    function (Crawler $node, $i) {
                        return [
                            str_replace('- ', '', $node->text()) =>
                                $node->extract(['href'])[0]
                        ];

                    }
                )
            ;

            if (!empty($nodeLinks)) {
                $nodeLinks = call_user_func_array('array_merge', $nodeLinks);
                $withLinks = array_merge($withLinks, $nodeLinks);
            } else {
                $io->warning('Empty search results for ' . $word . '.');
                $nonLinks [] = $word;
            }

            $io->progressAdvance();
        }
        $io->progressFinish();
        file_put_contents('data/reference/node_links.yaml', Yaml::dump($withLinks));
        file_put_contents('data/reference/non_links.yaml', Yaml::dump($nonLinks));
        file_put_contents('data/reference/node_links_unique.yaml', Yaml::dump(array_unique($withLinks)));
        $io->success('Done');
    }

    protected function getDictionary($dictionaryName) {
        $wordTokenizer = new WordTokenizer();
        $nameWords = $wordTokenizer->tokenize($dictionaryName);
        $shortName = mb_convert_case(
            implode(
                array_map(
                    function($v) {
                        if(!is_numeric($v)) {
                            return substr($v, 0, 1);
                        }
                    }, $nameWords
                )
            )
            , MB_CASE_UPPER, "UTF-8"
        );
        $dictionary = $this->em->getRepository(BibleDictionary::class)
            ->findOneBy(['shortName' => $shortName]);
        if (is_null($dictionary)) {
            $dictionary = new BibleDictionary();
            $dictionary->setPublishDate(new \DateTime());
            $dictionary->setVersionDate($dictionary->getPublishDate());
            $dictionary->setVersion(1.0);
            $dictionary->setCanonicalName(mb_convert_case($dictionaryName, MB_CASE_TITLE, "UTF-8"));
            $dictionary->setShortName(mb_convert_case($shortName, MB_CASE_UPPER, "UTF-8"));
            $dictionary->setStrong(false);
            $dictionary->setRightToLeft(false);
            $dictionary->setDescription(
                "Scrapped from <a href=\"$this->baseUrl\">Biblia Todo</a>."
            );
            $dictionary->setComments($dictionary->getDescription());
            $dictionary->setAuthor('N/A');
            $this->em->persist($dictionary);
            $this->em->flush();
        }
        return $this->em->getRepository(BibleDictionary::class)
            ->findOneBy(['shortName' => $shortName]);
    }

    /**
     * @return mixed
     */
    protected function getWordsList()
    {
        return Yaml::parse(
            file_get_contents(
                realpath('data/reference/dictionary_words.yaml')
            )
        )['dictionary_words'];
    }

    /**
     * @param SymfonyStyle $io
     * @param string $searchQuery
     */
    protected function scrapOnline(SymfonyStyle $io, $searchQuery = '')
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);

        $crawler = $goutteClient->request('GET', $this->baseUrl.'/'.'?s='.$searchQuery);

        $nodeLinks = $crawler->filterXPath('//*[@id="grupo"]')
            ->filter('p')
            ->filter('a')
            ->each(
                function (Crawler $node, $i) {
                    return [str_replace('- ', '', $node->text()) => $node->extract(['href'])[0]];

                }
            )
        ;

        if (!empty($nodeLinks)) {
            $nodeLinks = call_user_func_array('array_merge', $nodeLinks);
        } else {
            $io->warning('Empty search results.');
            exit();
        }
        $question = new ChoiceQuestion(
            'Please select an option.',
            array_keys($nodeLinks),
            0
        );
        $question->setErrorMessage('Option %s is invalid.');

        $link = $io->askQuestion($question);


        $crawler = $goutteClient->request('GET', $nodeLinks[$link]);

        $resultsContent = $crawler->filterXPath('//*[@id="imprimible"]')->children()
            ->each(
                function (Crawler $node, $i ) {
                    if(!empty($node->html())) {
                        return [$node->nodeName() => trim($node->text())];
                    }
                }
            );

        $resultsContent = array_values(array_filter($resultsContent));

        $hydratedContent = [];
        foreach ($resultsContent as $resultContent) {
            if(array_key_exists('h5', $resultContent)) {
                $title = $resultContent['h5'];
            } elseif(array_key_exists('p', $resultContent)) {
                $hydratedContent[$title][] = $resultContent['p'];
            }
        }

        foreach ($hydratedContent as $title => $paragraphs) {
            $io->title($title);

            foreach ($paragraphs as $paragraph) {
                $io->text($paragraph);
            }
        }
    }
}
