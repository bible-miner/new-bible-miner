<?php

namespace App\Command;

use App\Entity\BibleDictionary;
use App\Entity\BibleDictionaryData;
use Doctrine\Bundle\DoctrineBundle\ConnectionFactory;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Phpml\Tokenization\WordTokenizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Wamania\Snowball as Stemmer;

class BibleDictionaryImportCommand extends Command
{
    protected static $defaultName = 'bible:dictionary:import';

    protected function configure()
    {
        $this
            ->setDescription('Import Dictionaries')
            ->addArgument(
                'datasource', InputArgument::OPTIONAL,
                'Datasource name (filename without extension)'
            )
            ->addOption(
                'short-name', null, InputOption::VALUE_REQUIRED,
                'Dictionary short name'
            )
        ;
    }

    /**
     * @var bool|string
     */
    protected $dataSourcePath;

    /**
     * @var ConnectionFactory
     */
    protected $connectionFactory;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct($name = null, ConnectionFactory $connectionFactory,
                                EntityManagerInterface $em)
    {
        parent::__construct($name);
        $this->dataSourcePath = realpath('data/source');
        $this->em = $em;
        $this->connectionFactory = $connectionFactory;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $connection = $this->connectionFactory->createConnection(
            ['pdo' => new \PDO("sqlite:" . $this->dataSourcePath .
                DIRECTORY_SEPARATOR . $input->getArgument('datasource') . '.dct.sqlite'
            )]
        );

        try {
            $stmt = $connection->executeQuery(
                "SELECT * FROM main.dictionary ORDER BY relativeorder ASC, word ASC"
            );

            $dictionaryData = $stmt->fetchAll(\PDO::FETCH_OBJ);
        } catch (DBALException $e) {
            $io->error($e->getMessage());
        }

        $dictionary = $this->em->getRepository(BibleDictionary::class)
            ->findOneBy(['shortName' => $input->getOption('short-name')]);

        if ($dictionary->getLanguage() == 'es') {
            $stemmer = new Stemmer\Spanish();
        } else {
            $io->error('Language not supported.');
            exit();
        }

        $wordTokenizer = new WordTokenizer();

        foreach ($dictionaryData as $record) {
            $dictionaryDataEntity = new BibleDictionaryData();
            $dictionaryDataEntity->setDictionary($dictionary);
            $dictionaryDataEntity->setReference(
                mb_convert_case($record->word, MB_CASE_TITLE, "UTF-8")
            );

            $wordsArray = array_map(
                'strtolower', $wordTokenizer->tokenize($record->word)
            );

            $stemmedReference = array_filter(array_map(
                function($word) use ($stemmer) {
                        return $stemmer->stem($word);
                }, $wordsArray
            ));

            $record->data = str_replace(
                ['\u243?', '\u233?'],
                ['ó', 'é', '', ''], $record->data
            );

            $record->data = preg_replace("/<\/?(a)(.|\s)*?>/is", "", $record->data);

            $dictionaryDataEntity->setStemmedReference(implode(" ", $stemmedReference));
            $dictionaryDataEntity->setDefinition($record->data);
            $this->em->persist($dictionaryDataEntity);
        }
        $this->em->flush();

        $io->success(sprintf('Dictionary %s successfully imported', $dictionary));
    }

    protected function getDictionary($shortName) {

    }
}
