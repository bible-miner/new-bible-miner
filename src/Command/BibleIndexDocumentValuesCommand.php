<?php

namespace App\Command;

use App\Entity\BibleKJ2000RawContent;
use App\Entity\BibleKJ2000VerseStemmedVSM;
use App\Entity\BibleKJ2000VerseVSM;
use App\Entity\BibleRVGRawContent;
use App\Entity\BibleText;
use App\Entity\BibleVerseStemmedVSM;
use App\Entity\BibleVerseVSM;
use App\Entity\EnglishStemmedVocabulary;
use App\Entity\EnglishVocabulary;
use App\Entity\SpanishStemmedVocabulary;
use App\Entity\SpanishVocabulary;
use App\Entity\StemmedVocabulary;
use App\Entity\Vocabulary;
use App\Helper\DatabaseHelper;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Phpml\Preprocessing\Normalizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BibleIndexDocumentValuesCommand extends Command
{
    protected static $defaultName = 'bible:index:document-values';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DatabaseHelper
     */
    protected $databaseHelper;

    public function __construct(?string $name = null, EntityManagerInterface $em, DatabaseHelper $databaseHelper)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->databaseHelper = $databaseHelper;
    }

    protected function configure()
    {
        $this
            ->setDescription('Calculate Term Frequency, Document Frequency(DF) and Collection Frequency (CF)')
            ->addArgument(
                'version', InputArgument::OPTIONAL,
                'Version shortname (\'RVG\', \'RVG1909\',..).',
                'RVG'
            )
            ->addOption(
                'stemmed', 's', InputOption::VALUE_OPTIONAL,
                'Use stemmed words.', 1
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $bibleRepository = $this->em->getRepository(BibleText::class);
        $vocabularyRepository = $this->em->getRepository(Vocabulary::class);
        $stemmedVocabularyRepository = $this->em->getRepository(StemmedVocabulary::class);
        $verseVSMEntity = new BibleVerseVSM();
        $stemmedVerseVSMEntity = new BibleVerseStemmedVSM();

        if($input->getOption('stemmed') == 1) {
            $verseFilteredTexts = array_map('current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.stemmedVerseTokens')->getQuery()->getArrayResult());
            $verseIds = array_map('current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.id')->getQuery()->getArrayResult());
            $vocabularyTokens = array_map('current', $stemmedVocabularyRepository->createQueryBuilder('sv')
                ->select('sv.stemmedWord')->getQuery()->getArrayResult());
            $vocabularyTokenIdfValues = array_map('current', $stemmedVocabularyRepository->createQueryBuilder('sv')
                ->select('sv.idfValue')->getQuery()->getArrayResult());
            $vocabularyTokenIds = array_map('current', $stemmedVocabularyRepository->createQueryBuilder('sv')
                ->select('sv.id')->getQuery()->getArrayResult());
            $vocabularyTokenIdfValues = array_combine($vocabularyTokens, $vocabularyTokenIdfValues);
            $vocabularyTokens = array_combine($vocabularyTokens, $vocabularyTokenIds);


            $io->warning('Truncating stemmed vector space model table.');
            $this->databaseHelper->truncateTable($stemmedVerseVSMEntity);
            $classMetaData = $this->em->getClassMetadata(get_class($stemmedVerseVSMEntity));

            $io->progressStart(count($verseIds));
            $sql = sprintf(
                "INSERT INTO `%s` (`verse_id`, `word_id`, `freq_value`, `tf_idf_value`) VALUES\n",
                $classMetaData->getTableName()
            );

            $c = 0;
            foreach ($verseIds as $k => $verseId) {
                $tokens = array_filter(explode(' ', $verseFilteredTexts[$k]));
                $tokens = array_count_values($tokens);
                foreach ($tokens as $token => $tokenFreq) {
                    $c++;
                    $sql .= sprintf(
                        "(%d, %d, %d, " . $vocabularyTokenIdfValues[$token] * $tokenFreq . "),\n",
                        $verseId, $vocabularyTokens[$token], $tokenFreq
                    );
                }
                $io->progressAdvance();
            }
            $io->progressFinish();
            $sql = rtrim($sql, ",\n") . ";\n";

            try {
                $io->text(sprintf('Commiting %d records to database...', $c));
                $this->em->getConnection()->executeUpdate($sql);
                $io->success(sprintf('Commited %d records to database...', $c));
            } catch (DBALException $e) {
                $io->error($e->getMessage());
                exit();
            }
        } else {
            $verseFilteredTexts = array_map('current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.verseTokens')->getQuery()->getArrayResult());
            $verseIds = array_map('current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.id')->getQuery()->getArrayResult());
            $vocabularyTokens = array_map('current', $vocabularyRepository->createQueryBuilder('sv')
                ->select('sv.word')->getQuery()->getArrayResult());
            $vocabularyTokenIdfValues = array_map('current', $vocabularyRepository->createQueryBuilder('sv')
                ->select('sv.idfValue')->getQuery()->getArrayResult());
            $vocabularyTokenIds = array_map('current', $vocabularyRepository->createQueryBuilder('sv')
                ->select('sv.id')->getQuery()->getArrayResult());
            $vocabularyTokenIdfValues = array_combine($vocabularyTokens, $vocabularyTokenIdfValues);
            $vocabularyTokens = array_combine($vocabularyTokens, $vocabularyTokenIds);


            $io->warning('Truncating vector space model table.');
            $this->databaseHelper->truncateTable($verseVSMEntity);
            $classMetaData = $this->em->getClassMetadata(get_class($verseVSMEntity));

            $io->progressStart(count($verseIds));
            $sql = sprintf(
                "INSERT INTO `%s` (`verse_id`, `word_id`, `freq_value`, `tf_idf_value`) VALUES\n",
                $classMetaData->getTableName()
            );

            $c = 0;
            foreach ($verseIds as $k => $verseId) {
                $tokens = array_filter(explode(' ', $verseFilteredTexts[$k]));
                $tokens = array_count_values($tokens);

                foreach ($tokens as $token => $tokenFreq) {
                    $c++;
                    $sql .= sprintf(
                        "(%d, %d, %d, " . $vocabularyTokenIdfValues[$token] * $tokenFreq . "),\n",
                        $verseId, $vocabularyTokens[$token], $tokenFreq
                    );
                }
                $io->progressAdvance();
            }
            $io->progressFinish();
            $sql = rtrim($sql, ",\n") . ";\n";

            try {
                $io->text(sprintf('Commiting %d records to database...', $c));
                $this->em->getConnection()->executeUpdate($sql);
                $io->success(sprintf('Commited %d records to database...', $c));
            } catch (DBALException $e) {
                $io->error($e->getMessage());
                exit();
            }
        }
    }
}
