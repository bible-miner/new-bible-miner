<?php

namespace App\Command;

use App\Entity\BibleText;
use App\Entity\StemmedVocabulary;
use App\Entity\Vocabulary;
use App\Helper\DatabaseHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BibleIndexTermValuesCommand extends Command
{
    protected static $defaultName = 'bible:index:term-values';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var DatabaseHelper
     */
    protected $databaseHelper;

    public function __construct(?string $name = null, EntityManagerInterface $em, DatabaseHelper $databaseHelper)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->databaseHelper = $databaseHelper;
    }

    protected function configure()
    {
        $this
            ->setDescription('Calculate Document Frequency(DF) and Collection Frequency (CF)')
            ->addArgument(
                'version', InputArgument::OPTIONAL,
                'Version shortname (\'RVG\', \'RVG1909\',..).',
                'RVG'
            )
            ->addOption(
                'stemmed', 's', InputOption::VALUE_OPTIONAL,
                'Use stemmed words.', 1
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $bibleRepository = $this->em->getRepository(BibleText::class);
        $vocabularyRepository = $this->em->getRepository(Vocabulary::class);
        $stemmedVocabularyRepository = $this->em->getRepository(StemmedVocabulary::class);

        if($input->getOption('stemmed') == 1) {
            $bibleVerseTokens = array_map(
                'current',
                $bibleRepository->createQueryBuilder('brc')->select('brc.stemmedVerseTokens')
                ->getQuery()->getArrayResult()
            );

            $bibleVerseTokensAll = array_filter(explode(' ', implode(' ', $bibleVerseTokens)));
            $cfValues = array_count_values($bibleVerseTokensAll);
            $totalVerses = count($bibleVerseTokens);

            $io->progressStart(count($cfValues));
            foreach ($cfValues as $word => $cfValue) {
                try {
                    $dfValue = $bibleRepository->createQueryBuilder('brc')
                        ->select('count(brc.id)')
                        ->where('brc.stemmedVerseTokens LIKE :wordM')
                        ->orWhere('brc.stemmedVerseTokens LIKE :wordB')
                        ->orWhere('brc.stemmedVerseTokens LIKE :wordE')
                        ->setParameter('wordM', '% ' . $word . ' %')
                        ->setParameter('wordB', $word . ' %')
                        ->setParameter('wordE', '% ' . $word)
                        ->getQuery()->getSingleScalarResult();
                } catch (NonUniqueResultException $e) {
                    $io->caution($e->getMessage());
                    exit($e->getCode());
                }
                /**
                 * @var StemmedVocabulary $vocabularyToken
                 */
                $vocabularyToken = $stemmedVocabularyRepository->findOneBy(['stemmedWord' => $word]);
                $vocabularyToken->setCollectionFreq($cfValue);
                $vocabularyToken->setDocumentFreq($dfValue);
                $vocabularyToken->setIdfValue(log($totalVerses/$dfValue,10));

                $this->em->persist($vocabularyToken);
                $io->progressAdvance();
                $this->em->flush();
            }

            $io->progressFinish();
        } else {
            $bibleVerseTokens = array_map(
                'current', $bibleRepository->createQueryBuilder('brc')
                ->select('brc.verseTokens')
                ->getQuery()->getArrayResult()
            );

            $bibleVerseTokensAll = array_filter(explode(' ', implode(' ', $bibleVerseTokens)));
            $cfValues = array_count_values($bibleVerseTokensAll);
            $totalVerses = count($bibleVerseTokens);

            $io->progressStart(count($cfValues));
            foreach ($cfValues as $word => $cfValue) {
                try {
                    $dfValue = $bibleRepository->createQueryBuilder('brc')
                        ->select('count(brc.id)')
                        ->where('brc.verseTokens LIKE :wordM')
                        ->orWhere('brc.verseTokens LIKE :wordB')
                        ->orWhere('brc.verseTokens LIKE :wordE')
                        ->setParameter('wordM', '% ' . $word . ' %')
                        ->setParameter('wordB', $word . ' %')
                        ->setParameter('wordE', '% ' . $word)
                        ->getQuery()->getSingleScalarResult();
                } catch (NonUniqueResultException $e) {
                    $io->caution($e->getMessage());
                    exit($e->getCode());
                }
                /**
                 * @var Vocabulary $vocabularyToken
                 */
                $vocabularyToken = $vocabularyRepository->findOneBy(['word' => $word]);
                $vocabularyToken->setCollectionFreq($cfValue);
                $vocabularyToken->setDocumentFreq($dfValue);
                $vocabularyToken->setIdfValue(log($totalVerses/$dfValue,10));

                $this->em->persist($vocabularyToken);
                $io->progressAdvance();
            }
            $this->em->flush();
            $io->progressFinish();
        }
    }
}
