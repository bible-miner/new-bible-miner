<?php

namespace App\Command;

use App\Entity\BibleBook;
use App\Entity\BibleText;
use App\Entity\BibleVersion;
use Doctrine\Bundle\DoctrineBundle\ConnectionFactory;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Phpml\Tokenization\WordTokenizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Wamania\Snowball as Stemmer;
use Phpml\FeatureExtraction\TextRank\Tool\StopWords as StopWords;

class BibleTextImportCommand extends Command implements ContainerAwareInterface
{
    protected static $defaultName = 'bible:text:import';

    /**
     * @var bool|string
     */
    protected $dataSourcePath;

    /**
     * @var ConnectionFactory
     */
    protected $connectionFactory;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Container
     */
    protected $container;

    public function __construct($name = null, ConnectionFactory $connectionFactory,
                                EntityManagerInterface $em)
    {
        parent::__construct($name);
        $this->dataSourcePath = realpath('data/source');
        $this->em = $em;
        $this->connectionFactory = $connectionFactory;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('Update bible miner database from data source.')
            ->addArgument(
                'datasource', InputArgument::REQUIRED,
                'Data source Name rvg, rv1909, rv1909.str, etc.'
            )
            ->addOption(
                'bible-version', null, InputOption::VALUE_REQUIRED,
                'Version short name.'
            )
            ->addOption(
                'language', null, InputOption::VALUE_OPTIONAL,
                'Bible text language', 'es'
            )
            ->addOption(
                'strong', null, InputOption::VALUE_OPTIONAL,
                'Version has strong numbers.', 0
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $connection = $this->connectionFactory->createConnection(
            ['pdo' => new \PDO("sqlite:" . $this->dataSourcePath .
                DIRECTORY_SEPARATOR . $input->getArgument('datasource') . '.bib.sqlite'
            )]
        );

        try {
            $stmt = $connection->executeQuery(
                "SELECT * FROM bible"
            );
            $bibleVerses = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $stmt = $connection->executeQuery(
                "SELECT COUNT(*) FROM bible"
            );
            $bibleVersesCount = $stmt->fetchAll(\PDO::FETCH_COLUMN)[0];
        } catch (DBALException $e) {
            $io->error($e->getMessage());
        }

        if ($input->getOption('language') == 'en') {
            $stemmer = new Stemmer\English();
            $stopWords = new StopWords\English();
        } elseif ($input->getOption('language') == 'es') {
            $stemmer = new Stemmer\Spanish();
            $stopWords = new StopWords\Spanish();
        } else {
            $io->error('Datasource language currently not suported.');
        }

        $bibleVersion = $this->em->getRepository(BibleVersion::class)
            ->findOneBy(['nameAbbreviation' => $input->getOption('bible-version')]);

        $io->text(sprintf('Processing %d verses...', count($bibleVerses)));
        $io->progressStart((int) $bibleVersesCount);

        foreach ($bibleVerses as $bibleVerse) {
            $theBible = new BibleText();

            if(property_exists($bibleVerse, 'ref')) {
                $reference = preg_split('/\s|:/', $bibleVerse->ref);
                $bibleBook = $this->em->getRepository(BibleBook::class)
                    ->findOneBy(['nameAbbreviation' => $reference[0]]);
                $theBible->setChapterNumber($reference[1]);
                $theBible->setVerseNumber($reference[2]);
                $theBible->setVerseReference($bibleVerse->ref);
            } else {
                $bibleBook = $this->em->getRepository(BibleBook::class)
                    ->find($bibleVerse->Book);
                $theBible->setChapterNumber($bibleVerse->Chapter);
                $theBible->setVerseNumber($bibleVerse->Verse);
                $theBible->setVerseReference(
                    $bibleBook->getNameAbbreviation() . ' ' .
                    $bibleVerse->Chapter . ':' . $bibleVerse->Verse
                );
            }

            $theBible->setBook($bibleBook);

//            $theBible->setVerseText(str_replace(['<r>','</r>', '<i>', '</i>'], '', $bibleVerse->verse));
            if(property_exists($bibleVerse, 'verse')) {
                $verseText = $bibleVerse->verse;
            } elseif(property_exists($bibleVerse, 'Scripture')) {
                $verseText = $bibleVerse->Scripture;
            }
            $theBible->setVerseText($verseText);

            $wordTokenizer = new WordTokenizer();
            $verseWordsArray = array_map(
                'strtolower', $wordTokenizer->tokenize($verseText)
            );

            $stemmedVerse = array_filter(array_map(
                function($word) use ($stemmer, $stopWords) {
                    if(!$stopWords->exist($word)) {
                        return $stemmer->stem($word);
                    }
                    return false;
                }, $verseWordsArray
            ));

            $verseTokens = array_filter(array_map(
                function($word) use ($stopWords) {
                    if(!$stopWords->exist($word)) {
                        return $word;
                    }
                    return false;
                }, $verseWordsArray
            ));

            $theBible->setVerseTokens(implode(" ", $verseTokens));
            $theBible->setStemmedVerseTokens(implode(" ", $stemmedVerse));
            $theBible->setVersion($bibleVersion);
            $theBible->setStrong((bool) $input->getOption('strong'));

            $this->em->persist($theBible);
            $io->progressAdvance();
        }
        $io->progressFinish();
        $io->success('Text Processed Successfully.');
        $io->caution('Committing to database.');
        $this->em->flush();

        $io->success('Datasource imported successfully');
    }
}
