<?php
/**
 * PHP version >= 7.0
 *
 * @category Fixture
 * @package  App\DataFixtures\ORM
 * @author   Rafael Ernesto Espinosa Santiesteban <rafael.espinosa@fluency-labs.com>
 * @license  MIT <http://www.opensource.org/licenses/mit-license.php>
 * @link     http://www.fluency-labs.com
 */

namespace App\DataFixtures\ORM;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class UserFixtures
 *
 * @category Fixture
 * @package  App\DataFixtures\ORM
 * @author   Rafael Ernesto Espinosa Santiesteban <rafael.espinosa@fluency-labs.com>
 * @license  MIT <http://www.opensource.org/licenses/mit-license.php>
 * @link     http://www.fluency-labs.com
 */
class UserFixtures extends Fixture implements ContainerAwareInterface
{

    /**
     * The container variable
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Sets container
     *
     * @param ContainerInterface|null $container The container
     *
     * @return void
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data from yaml file
     *
     * @return array
     */
    protected function loadData()
    {
        return Yaml::parse(
            file_get_contents(
                realpath(__DIR__ . '/../fixtures/users.yaml')
            )
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager Object Manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $data = $this->loadData();

        $userManager = $this->container->get('fos_user.user_manager');

        foreach ($data[User::class] as $k => $row) {
            /**
             * @var $user User
             */
            $user = $userManager->createUser();
            $user->setUsername($row['username']);
            $user->setEmail($row['email']);
            $user->setPlainPassword($row['password']);
            $user->setPhoneNumber($row['phoneNumber']);
            $user->setEnabled(true);
            $user->setRoles($row['roles']);
            $user->setFirstName($row['firstName']);
            $user->setLastName($row['lastName']);
            $user->setImage($row['image']);
            $user->setUpdatedAt(new \DateTime('now'));
            $userManager->updateUser($user, true);
        }
    }
}