<?php
/**
 * PHP version >= 7.0
 *
 * @category Fixture
 * @package  App\DataFixtures\ORM
 * @author   Rafael Ernesto Espinosa Santiesteban <rafael.espinosa@fluency-labs.com>
 * @license  MIT <http://www.opensource.org/licenses/mit-license.php>
 * @link     http://www.fluency-labs.com
 */

namespace App\DataFixtures\ORM;

use App\Entity\BibleBook;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class UserFixtures
 *
 * @category Fixture
 * @package  App\DataFixtures\ORM
 * @author   Rafael Ernesto Espinosa Santiesteban <rafael.espinosa@fluency-labs.com>
 * @license  MIT <http://www.opensource.org/licenses/mit-license.php>
 * @link     http://www.fluency-labs.com
 */
class BibleBookFixtures extends Fixture implements ContainerAwareInterface
{

    /**
     * The container variable
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Sets container
     *
     * @param ContainerInterface|null $container The container
     *
     * @return void
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data from yaml file
     *
     * @return array
     */
    protected function loadData()
    {
        return Yaml::parse(
            file_get_contents(
                realpath(__DIR__ . '/../fixtures/bible_books.yaml')
            )
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager Object Manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $data = $this->loadData();

        foreach ($data[BibleBook::class] as $k => $row) {
            /**
             * @var $bibleBook BibleBook
             */
            $bibleBook = new BibleBook();
            $bibleBook->setCanonicalName($row['canonicalName']);
            $bibleBook->setNameAbbreviation($row['nameAbbreviation']);
            $manager->persist($bibleBook);
            $this->addReference(md5($bibleBook->getNameAbbreviation()), $bibleBook);
            $manager->flush();
        }
    }
}