<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BibleVersionRepository")
 */
class BibleVersion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $canonicalName;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $nameAbbreviation;

    /**
     * @var string
     * @ORM\Column(type="string", length=2)
     */
    protected $language = 'es';

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCanonicalName(): ?string
    {
        return $this->canonicalName;
    }

    public function setCanonicalName(string $canonicalName): self
    {
        $this->canonicalName = $canonicalName;

        return $this;
    }

    public function getNameAbbreviation(): ?string
    {
        return $this->nameAbbreviation;
    }

    public function setNameAbbreviation(string $nameAbbreviation): self
    {
        $this->nameAbbreviation = $nameAbbreviation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }
}
