<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BibleVerseStemmedVSMRepository")
 */
class BibleVerseStemmedVSM
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var BibleText
     * @ORM\ManyToOne(targetEntity="BibleText")
     * @ORM\JoinColumn(name="verse_id", referencedColumnName="id")
     */
    protected $verse;

    /**
     * @var StemmedVocabulary
     * @ORM\ManyToOne(targetEntity="StemmedVocabulary")
     * @ORM\JoinColumn(name="word_id", referencedColumnName="id")
     */
    protected $word;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, options={"default"="0"})
     */
    protected $freqValue;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true, options={"default"="0"})
     */
    protected $tfIdfValue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFreqValue(): ?int
    {
        return $this->freqValue;
    }

    public function setFreqValue(?int $freqValue): self
    {
        $this->freqValue = $freqValue;

        return $this;
    }

    public function getTfIdfValue(): ?float
    {
        return $this->tfIdfValue;
    }

    public function setTfIdfValue(?float $tfIdfValue): self
    {
        $this->tfIdfValue = $tfIdfValue;

        return $this;
    }

    public function getVerse(): ?BibleText
    {
        return $this->verse;
    }

    public function setVerse(?BibleText $verse): self
    {
        $this->verse = $verse;

        return $this;
    }

    public function getWord(): ?StemmedVocabulary
    {
        return $this->word;
    }

    public function setWord(?StemmedVocabulary $word): self
    {
        $this->word = $word;

        return $this;
    }
}
