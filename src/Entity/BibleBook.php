<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BibleBookRepository")
 */
class BibleBook
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $canonicalName;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $nameAbbreviation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCanonicalName(): ?string
    {
        return $this->canonicalName;
    }

    public function setCanonicalName(string $canonicalName): self
    {
        $this->canonicalName = $canonicalName;

        return $this;
    }

    public function getNameAbbreviation(): ?string
    {
        return $this->nameAbbreviation;
    }

    public function setNameAbbreviation(string $nameAbbreviation): self
    {
        $this->nameAbbreviation = $nameAbbreviation;

        return $this;
    }

}
