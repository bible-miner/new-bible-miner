<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BibleDictionaryRepository")
 */
class BibleDictionary
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=64)
     */
    protected $canonicalName;

    /**
     * @var string
     * @ORM\Column(type="string", length=8)
     */
    protected $shortName;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $comments;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     */
    protected $author;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $strong;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $version;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $versionDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $publishDate;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $rightToLeft;

    /**
     * @var string
     * @ORM\Column(type="string", length=2)
     */
    protected $language = 'es';

    public function __toString()
    {
        return $this->getShortName() . ' - ' . $this->getCanonicalName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCanonicalName(): ?string
    {
        return $this->canonicalName;
    }

    public function setCanonicalName(string $canonicalName): self
    {
        $this->canonicalName = $canonicalName;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getStrong(): ?bool
    {
        return $this->strong;
    }

    public function setStrong(bool $strong): self
    {
        $this->strong = $strong;

        return $this;
    }

    public function getVersion(): ?float
    {
        return $this->version;
    }

    public function setVersion(float $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getVersionDate(): ?\DateTimeInterface
    {
        return $this->versionDate;
    }

    public function setVersionDate(\DateTimeInterface $versionDate): self
    {
        $this->versionDate = $versionDate;

        return $this;
    }

    public function getPublishDate(): ?\DateTimeInterface
    {
        return $this->publishDate;
    }

    public function setPublishDate(\DateTimeInterface $publishDate): self
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    public function getRightToLeft(): ?bool
    {
        return $this->rightToLeft;
    }

    public function setRightToLeft(bool $rightToLeft): self
    {
        $this->rightToLeft = $rightToLeft;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }
}
