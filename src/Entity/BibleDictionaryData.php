<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BibleDictionaryDataRepository")
 */
class BibleDictionaryData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=127)
     */
    protected $reference;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $stemmedReference;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $definition;

    /**
     * @var BibleDictionary
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\BibleDictionary")
     * @ORM\JoinColumn(name="dictionary_id", referencedColumnName="id")
     */
    protected $dictionary;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    public function setDefinition(string $definition): self
    {
        $this->definition = $definition;

        return $this;
    }

    public function getDictionary(): ?BibleDictionary
    {
        return $this->dictionary;
    }

    public function setDictionary(?BibleDictionary $dictionary): self
    {
        $this->dictionary = $dictionary;

        return $this;
    }

    public function getStemmedReference(): ?string
    {
        return $this->stemmedReference;
    }

    public function setStemmedReference(string $stemmedReference): self
    {
        $this->stemmedReference = $stemmedReference;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }


}
