<?php

namespace App\Helper;

use App\Entity\BibleKJ2000VerseStemmedVSM;
use App\Entity\BibleVerseStemmedVSM;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class SearchHelper
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    public function getStemmedTermsScore($stemmedSearchTerms)
    {
        $connection = $this->entityManager->getConnection();
        $queryBuilder = $connection->createQueryBuilder();
        try {
            $queryBuilder->select(
                'sv.stemmed_word, sv.collection_freq AS cf',
                'sv.document_freq AS df, sv.idf_value AS idf'
            )
                ->from("stemmed_vocabulary", 'sv')
                ->where($queryBuilder->expr()->in('sv.stemmed_word', ':words'))
                ->setParameter('words', $stemmedSearchTerms, Connection::PARAM_STR_ARRAY)
                ->orderBy('idf', 'ASC');

            $result = $queryBuilder->execute()->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE);

            return $result;

        } catch (DBALException $e) {

        }
    }

    public function verseStemmedSuperRankedSearch($stemmedSearchTerms,  $limit = null, $offset = 0)
    {
        $mainQb = $this->getSuperRankedSearchQueryBuilder($stemmedSearchTerms);

        if(!is_null($limit)) {
            $mainQb->setMaxResults($limit)->setFirstResult($offset);
        }

        return $mainQb->execute()->fetchAll();
    }

    public function getSuperRankedSearchQueryBuilder($stemmedSearchTerms, $bibleVersion = 1) {
        $connection = $this->entityManager->getConnection();
        $subQb = $connection->createQueryBuilder();
        $mainQb = $connection->createQueryBuilder();

        $subQb->select(
            "bvsv2.verse_id,
                 GROUP_CONCAT(DISTINCT sv.stemmed_word SEPARATOR ',') AS words,
                 COUNT(*) AS CountOf, SUM(sv.idf_value) AS SumOf"
        )
        ->from("bible_verse_stemmed_vsm", 'bvsv2')
        ->innerJoin(
            'bvsv2', "stemmed_vocabulary", 'sv',
            'sv.id = bvsv2.word_id'
        )
        ->where(
            $subQb->expr()->in(
                'sv.stemmed_word', ':words'
            )
        )
        ->groupBy('bvsv2.verse_id')
        ->having('COUNT(*) > 1');

        $mainQb->select(
            "brc.id as verse_id, brc.verse_reference AS ref, 
            brc.book_id AS book_id, brc.chapter_number AS chapter_number, brc.verse_number AS verse_number,
            brc.verse_text AS verse_text, v2.words AS words, 
            bb.canonical_name AS bibleBookCanonicalName, brc.version_id AS bible_version_id,
            bv.canonical_name AS bibleVersionCanonicalName,
            v2.CountOf AS matches, v2.SumOf AS score"
        )
        ->from("bible_verse_stemmed_vsm", 'bvsv')
        ->innerJoin(
            'bvsv', "bible_text",
            'brc', 'brc.id = bvsv.verse_id'
        )
        ->innerJoin(
            'brc', "bible_book",
            'bb', 'bb.id = brc.book_id'
        )->innerJoin('brc', 'bible_version', 'bv', 'bv.id = brc.version_id')
        ->innerJoin(
            'bvsv', sprintf('(%s)', $subQb->getSQL()), 'v2',
            'v2.verse_id = bvsv.verse_id'
        )
        ->where('brc.version_id = :bibleVersion')
        ->setParameter('words', $stemmedSearchTerms, Connection::PARAM_STR_ARRAY)
        ->setParameter('bibleVersion', $bibleVersion)
        ->groupBy('brc.id')
        ->orderBy('v2.CountOf', 'DESC')
        ->addOrderBy('v2.SumOf', 'DESC');

        return $mainQb;
    }

    public function getBookChaptersQueryBuilder($bookId)
    {
        $connection = $this->entityManager->getConnection();

        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder->select('DISTINCT brc.chapter_number')
            ->from("bible_text", 'brc')
            ->where($queryBuilder->expr()->eq('brc.book_id', $bookId));
        return $queryBuilder;
    }

    public function getChapterVersesQueryBuilder($bookId, $chapterNumber)
    {
        $connection = $this->entityManager->getConnection();

        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder->select('DISTINCT brc.verse_number')
            ->from("bible_text", 'brc')
            ->where($queryBuilder->expr()->eq('brc.chapter_number', $chapterNumber))
            ->andWhere($queryBuilder->expr()->eq('brc.book_id', $bookId));
        return $queryBuilder;

    }

    public function calculateVersePage($bookId, $chapter, $verse, $versionId)
    {
        $connection = $this->entityManager->getConnection();

        $queryBuilder = $connection->createQueryBuilder();

        $queryBuilder->select('CONCAT(brc.chapter_number, ":", brc.verse_number)')
            ->from("bible_text", 'brc')
            ->where($queryBuilder->expr()->eq('brc.book_id', $bookId))
            ->andWhere($queryBuilder->expr()->eq('brc.version_id', $versionId));
        $result = $queryBuilder->execute()->fetchAll(\PDO::FETCH_COLUMN);
        $page = (int) ceil((array_search("{$chapter}:{$verse}", $result) + 1)/15);

        return $page;
    }
}