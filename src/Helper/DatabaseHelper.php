<?php

namespace App\Helper;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DatabaseHelper implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function truncateTable($entityClass) {
        $classMetadata = $this->entityManager->getClassMetadata(get_class($entityClass));
        $connection = $this->entityManager->getConnection();
        $connection->beginTransaction();

        try {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $connection->query('DELETE FROM '.$classMetadata->getTableName());
            // Beware of ALTER TABLE here--it's another DDL statement and will cause
            // an implicit commit.
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->query('ALTER TABLE '. $classMetadata->getTableName() .' AUTO_INCREMENT = 1');
            $connection->commit();
        } catch (DBALException $e) {
            try {
                $connection->rollback();
            } catch (ConnectionException $e) {
            }
        }

    }

    public function getNonIndexedVerses($stemmed = false)
    {
        $connection = $this->entityManager->getConnection();
        try {
            if($stemmed == true) {
                $sql = "
                    SELECT A.verse_id + 1
                    FROM bible_verse_stemmed_vsm AS A
                    WHERE NOT EXISTS (
                    SELECT B.verse_id FROM bible_verse_stemmed_vsm AS B
                    WHERE A.verse_id + 1 = B.verse_id)
                    GROUP BY A.verse_id;
                ";
                $stmt = $connection->prepare($sql);
            } else {
                $sql = "
                    SELECT A.verse_id + 1
                    FROM bible_verse_vsm AS A
                    WHERE NOT EXISTS (
                    SELECT B.verse_id FROM bible_verse_vsm AS B
                    WHERE A.verse_id + 1 = B.verse_id)
                    GROUP BY A.verse_id;
                ";

                    $stmt = $connection->prepare($sql);
            }
            $stmt->execute();
        } catch (DBALException $e) {

        }

        return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }
}