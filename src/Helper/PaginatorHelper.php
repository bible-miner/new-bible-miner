<?php

namespace App\Helper;

use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineDbalAdapter;
use Pagerfanta\Pagerfanta;


class PaginatorHelper
{
    private const MAX_ITEMS = 15;

    /**
     * Creates a Doctrine ORM paginator for the given query builder.
     *
     * @param DoctrineQueryBuilder $queryBuilder
     * @param int                                $page
     * @param int                                $maxPerPage
     *
     * @return Pagerfanta
     */
    public function createDbalPaginator($queryBuilder, $page = 1, $maxPerPage = self::MAX_ITEMS)
    {
        $countQueryBuilderModifier = function ($queryBuilder) {
            $queryBuilder->select('COUNT(DISTINCT brc.id) AS total_results')
                ->setMaxResults(1);
        };

        // don't change the following line (you did that twice in the past and broke everything)
        $paginator = new Pagerfanta(new DoctrineDbalAdapter($queryBuilder, $countQueryBuilderModifier));
        $paginator->setMaxPerPage($maxPerPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function createArrayPaginator($arrayRecords, $page = 1, $maxPerPage = self::MAX_ITEMS)
    {
        // don't change the following line (you did that twice in the past and broke everything)
        $paginator = new Pagerfanta(new ArrayAdapter($arrayRecords));
        $paginator->setMaxPerPage($maxPerPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}